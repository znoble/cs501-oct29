#!/usr/bin/env python3

import sys
import os
import numpy as np
from random import randrange,shuffle
from scipy.special import expit
import matplotlib.pyplot as plt

alphas = [0.001, 0.002, 0.005,0.01, 0.02, 0.05, 0.1, 0.2, 0.5]


def readpgmfile(filename):
    with open(filename,'r') as fd:
       magic = fd.readline().strip('\n')
       
       if magic != "P2":
           print("Image files is not a PGM Files. Please convert it to a PGM.", file=sys.stderr)
           sys.exit(1)

       wid, hgt  = tuple(map(int, fd.readline().strip().split()))
       pmax  = int(fd.readline())
       
       a = []
       for line in fd.readlines():
          a += list(map(int, line.strip().split()))

    return a, hgt, wid,pmax

    
if __name__ == "__main__":

# store amount right and amout wrong after running with testing data
# [alpha, right, wrong, percent right]
    rw_test = []

# set plt figure
# handles list stores learning curves for legend
    plt.figure(figsize=(16,9), dpi=80)
    plt.style.use("seaborn-notebook")
    handles = []


# read file, first two columns are test results
#    third column indicates whether or not they had disease
    print("Reading Image")
    test, h, w, pmax = readpgmfile("digits.pgm")

    print("Processing Image")

    im1 = np.array(test,dtype=np.float).reshape((50,20,100,20))
    im2 = im1.transpose((0,2,1,3))
    im3 = im2.reshape(10,500,400)

    train = im3[ : , :400 , : ]     # 80% of the data for training
    test  = im3[ : , 400: , : ]     # 20% of the data for training

# random starting weights matrix:
# the values of w are what we are 'learning'

    w = np.random.randn(4000).reshape(10,400)

# some data to keep track of how we are doing

    

    picklist = [(i,j) for i in range(10) for j in range(400)]
    shuffle(picklist)
        
    for alpha in alphas:
        tr_right = tr_wrong = 0
        print("Training with alpha = {0}".format(alpha))
        rw_train = []

        for reps in range(10):
            for whichdigit,whichimage in picklist:

                x = train[whichdigit,whichimage]  # getting 400 pixels for this picture

# make a one-hot vector:
#      if right answer is 0, the vector is [1,0] (1 in the 0 position)
#      if right answer is 1, the vector is [0,1] (1 in the 1 position)

                y = np.zeros(10)           # make a vector of zeros
                y[whichdigit] = 1.0        # set the answer component to 1

                z = np.matmul(w,x)        # forward propogation
                a = expit(z)              # logistic function

                err = a - y               # error: output - answer
                total_err = sum(err**2)
                yans = np.argmax(a)       # which component is larger

                if yans == whichdigit:    # right or wrong?
                    tr_right += 1
                else:
                    tr_wrong += 1

                dw = np.outer(err,x)
                w -= alpha * dw           # learning rate

                rw_train.append(tr_right / (tr_right+tr_wrong))  # store ratio of correct to total
                
 
# plot the rw_train for each alpha
# handles stores the lines for the plt legend
        line, = plt.plot(rw_train, label=str(alpha))
        handles.append(line)

#
# test the trained network on data it has never seen
#
        print("Testing with alpha =  {0}".format(alpha))
        te_right = te_wrong = 0
        for i in range(10):        # for each digit
            for j in range(100):   # 100 pictures of each digit in the test data
                x = test[i,j]
                y = np.zeros(10)           # make a vector of zeros
                y[i] = 1.0                 # set the answer component to 1

                z = np.matmul(w,x)         # forward propogation
                a = expit(z)               # logistic function
                guess = np.argmax(a)       # which component is larger

                if guess == i:
                    te_right += 1
                else:
                    te_wrong += 1

#
# store the testing data for later use
#
        rw_test.append([alpha, te_right, te_wrong, "{0:.2f}%".format(float((te_right / (te_right + te_wrong)) * 100))])

#
# Plot Formating and Display
#
    plt.xticks([])
    plt.title("Learning Curves of Digit Recognition Neural Network (Training Data)")
    plt.legend(handles=handles, title="Learning Rates")
    plt.table(cellText=rw_test, 
              colLabels=["Learning Rate", "# Right (Testing Data)", "# Wrong (Testing Data)", "Percent Correct (Testing Data)"], 
              loc="bottom", 
              label="Score of Network on Testing Data")
    plt.subplots_adjust(bottom=0.25)
    plt.savefig(os.getenv("HOME") + "/public_html/learning.png")